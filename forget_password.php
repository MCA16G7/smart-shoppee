<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->



<?php

require 'class.phpmailer.php';//path to php mailer class
require 'class.smtp.php';
require 'PHPMailerAutoload.php';
$mail = new PHPMailer();
include("db.php");
?>
<!DOCTYPE html>
<html>
<head>
<title>smart shoppee</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Super Market Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- font-awesome icons -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<!-- js -->
<script src="js/jquery-1.11.1.min.js"></script>
<!-- //js -->
<link href='//fonts.googleapis.com/css?family=Raleway:400,100,100italic,200,200italic,300,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
</head>
	
<body>
<!-- header -->
	<div class="agileits_header">
		<div class="container">
			<div class="w3l_offers">
				<p>Mega offer sales started....Dont miss it..... <a href="user_home.php">SHOP NOW</a></p>
			</div>
			<div class="agile-login">
				<ul>
					<li><a href="user_reg.php"> Create Account </a></li>
					<li><a href="login.php">Login</a></li>
					<li><a href="contact.php">Help</a></li>
<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account<b class="caret"></b></a>
	<ul class="dropdown-menu multi-column columns-3">
		<div class="row">
			<div class="multi-gd-img">
				<ul class="multi-column-dropdown">

					<li class="dropdown"><a href="profile.php"> View Profile </a>
					</li>
					<li><a href="notifications.php"> Notifications </a>
					</li>
					<li><a href="smart_wallet.php"> Smart Wallet </a>
					</li>
					<li><a href="mypurchase.php">My Purchase</a>
					</li>
					<li><a href="contact.php">Contact Us</a>
					</li>

				</ul>
			</div>

		</div>
	</ul>
</li>
					
					
				</ul>
			</div>
			<div class="product_list_header">  
					<form action="#" method="post" class="last"> 
						<input type="hidden" name="cmd" value="_cart">
						<input type="hidden" name="display" value="1">
						<button class="w3view-cart" type="submit" name="submit" value=""><i class="fa fa-cart-arrow-down" aria-hidden="true"></i></button>
					</form>  
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>

	<div class="logo_products">
		<div class="container">
		<div class="w3ls_logo_products_left1">
				<ul class="phone_email">
					<li><i class="fa fa-phone" aria-hidden="true"></i>Order online or call us : 8089503592</li>
					
				</ul>
			</div>
			<div class="w3ls_logo_products_left">
				<h1><a href="user_home.php">Smart Shoppee</a></h1>
			</div>
		<div class="w3l_search">
			<form action="#" method="post">
				<input type="search" name="Search" placeholder="Search for a Product..." required="">
				<button type="submit" class="btn btn-default search" aria-label="Left Align">
					<i class="fa fa-search" aria-hidden="true"> </i>
				</button>
				<div class="clearfix"></div>
			</form>
		</div>
			
			<div class="clearfix"> </div>
		</div>
	</div>
<!-- //header -->
<!-- navigation -->
		<div class="navigation-agileits">
		<div class="container">
			<nav class="navbar navbar-default">
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header nav_2">
								<button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div> 
							
							
							
							
							
							<div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
								<ul class="nav navbar-nav">
									<!--<li class="active"><a href="user_home.php" class="act">Home</a></li>	-->
									<!-- Mega Menu -->
									
									<li><a href="user_home.php">Home</a></li>
									<?php
									include("user_header.php");
									?>
									
									
								</ul>
							</div>
							</nav>
			</div>
		</div>
		
		
<!-- //navigation -->
<!-- breadcrumbs -->
	<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="user_home.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
				<li class="active">Login Page</li>
			</ol>
		</div>
	</div>
<!-- //breadcrumbs -->
<!-- login -->
	<div class="login">
		<div class="container">
			<h2>Forget password</h2>
		
			<div class="login-form-grids animated wow slideInUp" data-wow-delay=".5s">
				<form method="post">
				<input type="text" name="uname" placeholder="Type Your User Name" required=" " ><br>
				
					<input type="email" name="email" placeholder="Type Your Regitered Email address" required=" " >
					
					
					<input type="submit" name="Reset_Pass" value="Reset Password">
					
				</form>
				
				
				
				
				
				
				
				
				
				
				
				  <?php
				if(isset($_POST['Reset_Pass']))
				{
					$email=$_POST['email'];
					$uname=$_POST['uname'];
					
					$sql="select * from login where user_name='$uname'";
					$done=mysqli_query($con,$sql);
					if(mysqli_num_rows($done)>0)
					{
						$r=mysqli_fetch_array($done);
						$pass=$r['password'];
						
						$mail->IsSMTP(); // enable SMTP
						$mail->SMTPAuth = true; // authentication enabled
						$mail->SMTPSecure = 'tls'; // secure transfer enabled REQUIRED for GMail
						$mail->Host = gethostbyname('smtp.gmail.com');
						$mail->Port = 587; // or 587
						$mail->IsHTML(true);
						$mail->Username = "smartshoppeekmct@gmail.com";
						$mail->Password ="smart.123";
						$mail->SetFrom($email);
						$mail->Subject = "Password Reset";
						$mail->Body = "Current Password - ".$pass;
						$mail->AddAddress($email);
						$mail->Send();
						
						?>
							<script>
								alert("Password Send to your mailID, Use That password and try to login once Again..");
								window.location.assign("login.php");				
							</script>
						<?php
						
						
					}
					else
					{
						echo "username does not exist...!!!";
					}
				}
				?>
			</div>
			<h4>For New People</h4>
			<p><a href="user_reg.php">Register Here</a> (Or) go back to <a href="user_home.php">Home<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a></p>
		</div>
	</div>
<!-- //login -->
<!-- //footer -->
	
	
<!-- //footer -->	
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
<!-- top-header and slider -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
<script src="js/minicart.min.js"></script>
<script>
	// Mini Cart
	paypal.minicart.render({
		action: '#'
	});

	if (~window.location.search.indexOf('reset=true')) {
		paypal.minicart.reset();
	}
</script>
<!-- main slider-banner -->
<script src="js/skdslider.min.js"></script>
<link href="css/skdslider.css" rel="stylesheet">
<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery('#demo1').skdslider({'delay':5000, 'animationSpeed': 2000,'showNextPrev':true,'showPlayButton':true,'autoSlide':true,'animationType':'fading'});
						
			jQuery('#responsive').change(function(){
			  $('#responsive_wrapper').width(jQuery(this).val());
			});
			
		});
</script>	
<!-- //main slider-banner --> 

</body>
</html>