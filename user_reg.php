<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php
include("db.php");
?>
<!DOCTYPE html>
<html>
<head>
<title>Smart Shoppee</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Super Market Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- font-awesome icons -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<!-- js -->
<script src="js/jquery-1.11.1.min.js"></script>
<!-- //js -->
<link href='//fonts.googleapis.com/css?family=Raleway:400,100,100italic,200,200italic,300,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>


<script type="text/javascript">
    // Restrict user input in a text field
    // create as many regular expressions here as you need:
    var digitsOnly = /[1234567890]/g;
    var integerOnly = /[0-9\.]/g;
    var alphaOnly = /[A-Za-z]/g;
    var usernameOnly = /[A-Za-z\._-]/g;

    function restrictInput(myfield, e, restrictionType, checkdot){
        if (!e) var e = window.event
        if (e.keyCode) code = e.keyCode;
        else if (e.which) code = e.which;
        var character = String.fromCharCode(code);

        // if user pressed esc... remove focus from field...
        if (code==27) { this.blur(); return false; }

        // ignore if the user presses other keys
        // strange because code: 39 is the down key AND ' key...
        // and DEL also equals .
        if (!e.ctrlKey && code!=9 && code!=8 && code!=36 && code!=37 && code!=38 && (code!=39 || (code==39 && character=="'")) && code!=40) 
		{
            if (character.match(restrictionType)) 
			{
                if(checkdot == "checkdot")
				{
                    return !isNaN(myfield.value.toString() + character);
                } 
				else 
				{
                    return true;
                }
            } 
			else 
			{
                return false;
            }
        }
    }
</script>


<!-- start-smoth-scrolling -->
</head>
	
<body>
<!-- header -->
	<div class="agileits_header">
		<div class="container">
			<div class="w3l_offers">
				<p>Mega offer sales started....Dont miss it..... <a href="index.php">SHOP NOW</a></p>
			</div>
			<div class="agile-login">
				<ul>
					<li><a href="user_reg.php"> Create Account </a></li>
					<li><a href="login.php">Login</a></li>
					
					
				</ul>
			</div>
			
			<div class="clearfix"> </div>
		</div>
	</div>

	<div class="logo_products">
		<div class="container">
		<div class="w3ls_logo_products_left1">
				<!--<ul class="phone_email">
					<li><i class="fa fa-phone" aria-hidden="true"></i>Order online or call us : +91 9061230650</li> -->
					
				</ul>
			</div>
			<div class="w3ls_logo_products_left">
				<h1><a href="index.php">SMART SHOPPEE</a></h1>
			</div>
		
			
			<div class="clearfix"> </div>
		</div>
	</div>
<!-- //header -->
<!-- navigation -->

		
<!-- //navigation -->
<!-- breadcrumbs -->
	<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="index.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
				<li class="active">Register Page</li>
			</ol>
		</div>
	</div>
<!-- //breadcrumbs -->
<!-- register -->
	<div class="register">
		<div class="container">
			<h2>Register Here</h2>
			<div class="login-form-grids">
			
					<?php /*?><h5>profile information</h5><?php */?>
				<?php /*?><form action="#" method="post">
					<input type="text" placeholder="First Name..." required=" " >
					<input type="text" placeholder="Last Name..." required=" " >
				</form>
				<div class="register-check-box">
					<div class="check">
						<label class="checkbox"><input type="checkbox" name="checkbox"><i> </i>Subscribe to Newsletter</label>
					</div>
				</div><?php */?>
				
				
				
				
				
				
				
				
				
				
				<h6>Register Here</h6>
					<form method="post">
					<!--<input type="text" placeholder="" required=" " >-->
						<input type="text" placeholder="First Name..."   required class="form-control"  onkeypress="return restrictInput(this, event, alphaOnly);" name="fname"></br>
						<input type="text" placeholder="Last Name..."  class="form-control"  onkeypress="return restrictInput(this, event, alphaOnly);"  name="lname" ></br>
						<textarea placeholder="Address..." required  name="addr" ></textarea></br>
						<input type="email" placeholder="Email..." required=" " name="email" ></br>
					
						<input type="number" placeholder="Phone No..." required onkeypress="return restrictInput(this, event, digitsOnly);"  maxlength="10"   min length="10" class="form-control" name="phone" ></br>
						<input type="text" placeholder="Username..." required  name="unm" ></br>
				
						<input type="password" placeholder="Password..."  name="psw"  ></br>
									
						
											
						<input type="submit" name="register" value="Register">
				</form>
				<?php
					if(isset($_POST['register']))
					{
						$fname=$_POST['fname'];
						$lname=$_POST['lname'];
						$addr=$_POST['addr'];
						$email=$_POST['email'];
						$phone=$_POST['phone'];
						$unm=$_POST['unm'];
						$psw=$_POST['psw'];
						
						$check="SELECT * FROM `login` WHERE  user_name='$unm'";
						$done=mysqli_query($con,$check);
						$k=mysqli_num_rows($done);
						
						$check2="SELECT * FROM `user_registration` WHERE  email='$email'";
						$done2=mysqli_query($con,$check2);
						$k2=mysqli_num_rows($done2);
						
						if($k>0)
						{
						echo("The Username is Already Exist");
						}
						else if($k2>0)
						{
							echo("The email is Already used");
						}
						else
						{
						
							$sql="select max(log_id) as mx from login";
							$exe=mysqli_query($con,$sql);
							$r=mysqli_fetch_array($exe);
						
							$max=$r['mx'];
						
							if($max==0)
							{
								$maxid=10001;
							}
							else
							{
								$maxid=$max+1;
							}
						
							$sql1="INSERT INTO `user_registration`(`user_id`, `fname`, `lname`, `email`, `address`, `phone`) VALUES ('$maxid','$fname','$lname','$email','$addr','$phone')";
							$exe1=mysqli_query($con,$sql1);
							
							$g="INSERT INTO `smart_wallet`(`uid`, `amount`) VALUES ('$maxid','0')";
							$y=mysqli_query($con,$g);
						
						
							
							$sql2="INSERT INTO `login`(`log_id`, `user_name`, `password`, `type`) VALUES ('$maxid','$unm','$psw','user')";
							$exe2=mysqli_query($con,$sql2);
							
							if($exe2 && $exe1)
							{
								?>
								<script>
									alert("Registration successfull");
									window.location.assign("login.php");
								</script>
								<?php
							}
							else{
								echo("An error occured !!!!!!!");
							
								}
						
						
							
						}
						
					}
						
					?>
			</div>
			<div class="register-home">
				<a href="index.php">Home</a>
			</div>
		</div>
	</div>
	
	
<!-- //register -->
<!-- //footer -->

<!-- //footer -->	
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
<!-- top-header and slider -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
<script src="js/minicart.min.js"></script>
<script>
	// Mini Cart
	paypal.minicart.render({
		action: '#'
	});

	if (~window.location.search.indexOf('reset=true')) {
		paypal.minicart.reset();
	}
</script>
<!-- main slider-banner -->
<script src="js/skdslider.min.js"></script>
<link href="css/skdslider.css" rel="stylesheet">
<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery('#demo1').skdslider({'delay':5000, 'animationSpeed': 2000,'showNextPrev':true,'showPlayButton':true,'autoSlide':true,'animationType':'fading'});
						
			jQuery('#responsive').change(function(){
			  $('#responsive_wrapper').width(jQuery(this).val());
			});
			
		});
</script>	
<!-- //main slider-banner --> 

</body>

</html>