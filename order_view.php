<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php

include("db.php");

?>
<!DOCTYPE html>
<html>
<head>
<title>SMART SHOPPEE</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Super Market Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- font-awesome icons -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<!-- js -->
<script src="js/jquery-1.11.1.min.js"></script>
<!-- //js -->
<link href='//fonts.googleapis.com/css?family=Raleway:400,100,100italic,200,200italic,300,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
</head>
	
<body>

<!-- header -->
	<div class="agileits_header">
		<div class="container">
			
			<div class="agile-login">
				
			</div>
			
			<div class="clearfix"> </div>
		</div>
	</div>
<div class="logo_products">
		<div class="container">
		<div class="w3ls_logo_products_left1">
			
				<!--<ul class="phone_email">
					<li><i class="fa fa-phone" aria-hidden="true"></i>Order online or call us : +91 9061230650</li>
				</ul>-->
			</div>
			<div class="w3ls_logo_products_left">
				<h1><a href="employee_home.php">SMART SHOPPEE</a></h1>
			</div>
		<!--<div class="w3l_search">
			<form action="#" method="post">
				<input type="search" name="Search" placeholder="Search for a Product..." required="">
				<button type="submit" class="btn btn-default search" aria-label="Left Align">
					<i class="fa fa-search" aria-hidden="true"> </i>
				</button>
				<div class="clearfix"></div>
			</form>
		</div>-->
			
			<div class="clearfix"> </div>
		</div>
	</div>
<!-- //header -->
<!-- navigation -->
		<div class="navigation-agileits">
		<div class="container">
			<nav class="navbar navbar-default">
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header nav_2">
								<button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div> 
							
							
							
							
							
							<div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
								<ul class="nav navbar-nav">
									<!--<li class="active"><a href="employee_home.php" class="act">Home</a></li>	-->
									<!-- Mega Menu -->
									
									<li><a href="billing.php">New Bill</a></li>
									<li><a href="old_bill.php">Old Bill</a></li>	
									<li><a href="add_products.php">Add New Products</a></li>
														
									<li><a href="product_management.php">Manage</a></li>
									<li><a href="product_management.php">Update Stock</a></li>					
											
									
									
										<li><a href="online_sales.php">View  Sales</a></li>
														<li><a href="order.php">View Orders</a></li>
														<li><a href="verify_return.php">View Return</a></li>
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">Settings<b class="caret"></b></a>
										<ul class="dropdown-menu multi-column columns-3">
											<div class="row">
												<div class="multi-gd-img">
													<ul class="multi-column-dropdown">
														
														<li><a href="contact.php">Contacts</a></li>
														<li><a href="wallet.php">Smart Wallet</a></li>
														<li><a href="about_us.php">About us</a></li>
														<li><a href="\smart shoppee\logout.php">Logout</a></li>
													</ul>
												</div>	
												
											</div>
										</ul>
									</li>
								</ul>
							</div>
							</nav>
			</div>
		</div>
		
<!-- //navigation -->




	<!-- main-slider -->

		
		
		</div>
	</div>

	
	<div class="top-brands">
		<div class="container">
	<form method="post">		
	<?php
	$bno=$_GET['bno'];
	?>
	<table class="timetable_sub" border="2" bordercolordark="#570002">
	
	<thead>
		<tr>
			<th colspan="5"> Order ID :<?php echo($bno)?></th>
		</tr>
		<tr></tr>
		<th>serial_no</th>
		<th>Product Name</th>
		<th>Price</th>
		<th>Quantity</th>
		<th>Total</th>
		</thead>
		<?php
	$t="select * from cart where oid=$bno";
	$e=mysqli_query($con,$t);
				while($w=mysqli_fetch_array($e))
				{
					
					?>
					<tr>
					<td><?php echo($w['idd'])?></td>		
					<td><?php echo($w['pname'])?></td>
					<td><?php echo($w['price'])?></td>
					<td><?php echo($w['qty'])?></td>	
					<td><?php echo($w['total'])?></td>
					</tr>						
					<?php
					
				}
				
	?>
	
	
	</table>
	<br>
	<input type="submit" name="print" value="Print Bill">
	<br>
	<?php
		if(isset($_POST['print']))
		{
			
			 $q="SELECT * FROM `order_tbl` WHERE oid='$bno'";
			$exe=mysqli_query($con,$q);
			$g=mysqli_fetch_array($exe);
			?>
			<script>
	window.location.assign("online_bill.php?k=<?php echo $bno ?>&uid=<?php echo($g['uid'])?>")
	</script>
			<?php
		}
			
			?>
	</form>		
			<div class="grid_3 grid_5">
				<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
					<ul id="myTab" class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#expeditions" id="expeditions-tab" role="tab" data-toggle="tab" aria-controls="expeditions" aria-expanded="true">Order Requests</a></li>
						<li role="presentation"><a href="#tours" role="tab" id="tours-tab" data-toggle="tab" aria-controls="tours">Dispatched Orders</a></li>
					</ul>
					<div id="myTabContent" class="tab-content">
						<div role="tabpanel" class="tab-pane fade in active" id="expeditions" aria-labelledby="expeditions-tab">
							<div class="agile-tp">
								
								
								
						<table class="timetable_sub" border="2" bordercolordark="#570002">
			<thead>
		  
		  
			<tr>
				<th>Order ID</th>
				<th >Total</th>
				<th>Customer Name</th>
				<th>Address</th>
				<th>Dispatch</th>
				
				

			</tr>
			</thead>
			<?php
		    $q="SELECT * FROM `order_tbl` WHERE stat='req' ";
			$exe=mysqli_query($con,$q);	
				
			while($r=mysqli_fetch_array($exe))
			{
				$que="select * from user_registration where user_id=$r[uid]";
				$exe2=mysqli_query($con,$que);
				while($r2=mysqli_fetch_array($exe2))
					{
					?>	
				<tr>
					<td><a href="order_view.php?bno=<?php echo $r['oid'];?>&s=req"><?php echo $r['oid'];?></td>
					<td><?php echo $r['totl'];?></td>
					<td><?php echo $r2['fname'];?></td>
					<td><?php echo $r2['address'];?></td>
					
				<td><a href="dispatch.php?mid=<?php echo $r['oid'];?>"><img src="images/delivery.png" width="30" height="30"></a></td>
				
				</tr>
		    <?php
					}
		    
			 
			}
				?>
			</table>		
			
								
								
								
								
							</div>
							
							
						</div>
						<div role="tabpanel" class="tab-pane fade" id="tours" aria-labelledby="tours-tab">
							<div class="agile-tp">
								
								
							</div>
							
								
									
									
									
									
									
									
									
			<table class="timetable_sub" border="2" bordercolordark="#570002">
			<thead>
		  
		  
			<tr>
				<th>Order ID</th>
				<th >Total</th>
				<th>Customer Name</th>
				<th>Address</th>
				<th>Delivered</th>
				
				

			</tr>
			</thead>
			<?php
		    $q="SELECT * FROM `order_tbl` WHERE stat='shipped'";
			$exe=mysqli_query($con,$q);	
				
			while($r=mysqli_fetch_array($exe))
			{
				$que="select * from user_registration where user_id=$r[uid]";
				$exe2=mysqli_query($con,$que);
				while($r2=mysqli_fetch_array($exe2))
					{
					?>	
				<tr>
					<td><a href="order_view.php?bno=<?php echo $r['oid'];?>&s=shipped"><?php echo $r['oid'];?></td>
					<td><?php echo $r['totl'];?></td>
					<td><?php echo $r2['fname'];?></td>
					<td><?php echo $r2['address'];?></td>
					
				<td><a href="delivered.php?mid=<?php echo $r['oid'];?>"><img src="images/deliverd.jpg" width="40" height="40"></a></td>
				
				</tr>
		    <?php
					}
		    
			 
			}
				?>
			</table>
									
									
									
									
									
								
								<div class="clearfix"> </div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	
	
	
	
	
	
	
	
<!-- //top-brands -->
 <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
         <a href="beverages.html"> <img class="first-slide" src="images/b1.jpg" alt="First slide"></a>
       
        </div>
        <div class="item">
         <a href="personalcare.html"> <img class="second-slide " src="images/b3.jpg" alt="Second slide"></a>
         
        </div>
        <div class="item">
          <a href="household.html"><img class="third-slide " src="images/b1.jpg" alt="Third slide"></a>
          
        </div>
      </div>
    
    </div><!-- /.carousel -->	
<!--banner-bottom-->
				<div class="ban-bottom-w3l">
					<div class="container">
					<div class="col-md-6 ban-bottom3">
							<div class="ban-top">
								<img src="images/p2.jpg" class="img-responsive" alt=""/>
								
							</div>
							<div class="ban-img">
								<div class=" ban-bottom1">
									<div class="ban-top">
										<img src="images/p3.jpg" class="img-responsive" alt=""/>
										
									</div>
								</div>
								<div class="ban-bottom2">
									<div class="ban-top">
										<img src="images/p4.jpg" class="img-responsive" alt=""/>
										
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="col-md-6 ban-bottom">
							<div class="ban-top">
								<img src="images/111.jpg" class="img-responsive" alt=""/>
								
								
							</div>
						</div>
						
						<div class="clearfix"></div>
					</div>
				</div>
<!--banner-bottom-->
<!--brands-->



<!--//brands-->
<!-- new -->


<!-- //new -->
<!-- //footer -->
	
	
<!-- //footer -->	
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<!-- top-header and slider -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
<script src="js/minicart.min.js"></script>
<script>
	// Mini Cart
	paypal.minicart.render({
		action: '#'
	});

	if (~window.location.search.indexOf('reset=true')) {
		paypal.minicart.reset();
	}
</script>
<!-- main slider-banner -->
<script src="js/skdslider.min.js"></script>
<link href="css/skdslider.css" rel="stylesheet">
<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery('#demo1').skdslider({'delay':5000, 'animationSpeed': 2000,'showNextPrev':true,'showPlayButton':true,'autoSlide':true,'animationType':'fading'});
						
			jQuery('#responsive').change(function(){
			  $('#responsive_wrapper').width(jQuery(this).val());
			});
			
		});
</script>	
<!-- //main slider-banner --> 
</body>
</html>