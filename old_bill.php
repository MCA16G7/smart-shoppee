<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php

include("db.php");

?>
<!DOCTYPE html>
<html>
<head>
<title>SMART SHOPPEE</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Super Market Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- font-awesome icons -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<!-- js -->
<script src="js/jquery-1.11.1.min.js"></script>
<!-- //js -->
<link href='//fonts.googleapis.com/css?family=Raleway:400,100,100italic,200,200italic,300,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<script>
function printData()
{
   var divToPrint=document.getElementById("printTable");
   newWin= window.open("");
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
   newWin.close();
}
</script>
<style>
.autodropdown{
	position:absolute;
	top:530px;
	left:145px;
}
.suggestresult{
	width:300px;
	list-style:none;
}
 
.suggestresult li{
	padding:5px;
	border:1px solid #333;
	border-top:0;
	cursor:pointer;
	background:#333;
	color:#fff;
	border-bottom:1px solid #666;
}

.suggestresult li:hover{
	background:#666;
	color:#ffffff;
	cursor:pointer;
}

</style>
<script src="autosuggestion.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="jquery.min.js"></script>
<script>
$(document).ready(function(){
	$('.abhijitscript').keyup(function(){
		var query_string = $.trim($(this).val());
		//alert(query_string.length);
		if(query_string != ''){
			$.ajax({
			type: "POST",
			url: "getItem_2.php",
			data: { name:query_string },
			success: function(data)
			{
				$('.suggestresult').html(data);
				$('.suggestresult li').click(function(){
					var return_value = $(this).text();
					
					
					$('.abhijitscript').attr('value', return_value); 
					$('.abhijitscript').val(return_value);
					$('.suggestresult').html('');
					
					//auto data get starting 
					var inp=return_value;
					//alert(inp);                    
					var data = {
						"action": inp
					};
						data = $(this).serialize() + "&" + $.param(data);
						$.ajax({		
									type: "POST",
									dataType: "json",
									url: "getdata.php", //Relative or absolute path to response.php file
									data: data,
									success: function(data) {     
										$("#price").val(data["price"]);
										document.getElementById("price").focus();
									}
								});
								return false;
					//auto gata get ending
					
					
				});
			}
		});
		}
	});
});
</script>
<!-- start-smoth-scrolling -->
</head>
	

<body>
<!-- header -->
	<div class="agileits_header">
		<div class="container">
			
			<div class="agile-login">
				
			</div>
			<div class="product_list_header">  
					<form action="#" method="post" class="last"> 
						<input type="hidden" name="cmd" value="_cart">
						<input type="hidden" name="display" value="1">
					</form>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
<div class="logo_products">
		<div class="container">
		<div class="w3ls_logo_products_left1">
				<!--<ul class="phone_email">
					<li><i class="fa fa-phone" aria-hidden="true"></i>Order online or call us : +91 9061230650</li>
				</ul>-->
			</div>
			<div class="w3ls_logo_products_left">
				<h1><a href="employee_home.php">SMART SHOPPEE</a></h1>
			</div>
		<!--<div class="w3l_search">
			<form action="#" method="post">
				<input type="search" name="Search" placeholder="Search for a Product..." required="">
				<button type="submit" class="btn btn-default search" aria-label="Left Align">
					<i class="fa fa-search" aria-hidden="true"> </i>
				</button>
				<div class="clearfix"></div>
			</form>
		</div>-->
			
			<div class="clearfix"> </div>
		</div>
	</div>
<!-- //header -->
<!-- navigation -->
			<div class="navigation-agileits">
		<div class="container">
			<nav class="navbar navbar-default">
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header nav_2">
								<button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div> 
							
							
							
							
							
							<div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
								<ul class="nav navbar-nav">
									<!--<li class="active"><a href="employee_home.php" class="act">Home</a></li>	-->
									<!-- Mega Menu -->
									
									<li><a href="billing.php">New Bill</a></li>
									<li><a href="old_bill.php">Old Bill</a></li>	
									<li><a href="add_products.php">Add New Products</a></li>
														
									
									<li><a href="product_management.php">Update Stock</a></li>					
											
									
									
										<li><a href="online_sales.php">View  Sales</a></li>
														<li><a href="verify_return.php">View Return</a></li>
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">Settings<b class="caret"></b></a>
										<ul class="dropdown-menu multi-column columns-3">
											<div class="row">
												<div class="multi-gd-img">
													<ul class="multi-column-dropdown">
														
														<li><a href="\smart shoppee\logout.php">Logout</a></li>
													</ul>
												</div>	
												
											</div>
										</ul>
									</li>
								</ul>
							</div>
							</nav>
			</div>
		</div>
		
	


		
<div class="top-brands">
		<div class="container">
		<h2>BILLING</h2><br>
		<form method="post">
		<table class="timetable_sub" border="2" bordercolordark="#570002" >
			
			<thead>
			<tr>
				
				<th colspan="2">Bill No </th>
				<th><input type="text" name="bno" placeholder="" class="form-control"  ></th>
				<th><input type="submit" name="fetch" value="Retrieve" ></th>
			</tr>
			</thead>	
		</table><br><br>
		
		
		<table class="timetable_sub" border="2" bordercolordark="#570002" id="printTable">
		
		<thead>
			<tr>
				<th colspan="4"><h3>Smart Shoppee</h3></th>
			</tr>
			<tr>
				
				<th>Product Name</th>
				<th>Price</th>
				<th>Quantity</th>
				<th >Total</th>
				
			</tr>
			</thead>
		
		
		<?php
			 if(isset($_POST['fetch']))
					{
						
						$bill_no=$_POST['bno'];
						$bill_details="select * from bill_details where bill_no='$bill_no'";
						$exe=mysqli_query($con,$bill_details);
						
				while($ar1=mysqli_fetch_array($exe))
				{
				
				?>
				<tr align="center">
					<td><?php echo $ar1['product_name'];?></td>
					<td><?php echo $ar1['price'];?></td>
					<td><?php echo $ar1['qty'];?></td>
					<td><?php echo $ar1['total'];?></td>
					
				</tr>		
				
				<?php
				}
				?>
				<?php
				$g="select * from bill_master where bill_no='$bill_no'";
				$w=mysqli_query($con,$g);
				$amnt=mysqli_fetch_array($w);
				?>
				
					<tr>
						<th><strong>Purchase Date : <?php echo $amnt['date'];?></strong></th>
						<th><strong>Grand Total : <?php echo $amnt['total_amount'];?>  Only..</strong></th>
						<th colspan="4">Customer Name : <?php echo $amnt['user_id'];?></th>
					</tr>
				<tr>
					<th colspan="4"> Thank you For choosing us... we expect your support again....
					
				</th>
				</tr>
					</table>
				<input type="button" value="Print" name="pr" onClick="printData();">
				<?php
				
				}
			
			?>
						
	
		
		
		</div>
	</div>


<script>
	
		function tot()
		{
			var price=document.getElementById("price").value;
			var qty=document.getElementById("qty").value;
			
			var res=parseFloat(price)*parseFloat(qty);
			if(!isNaN(res))
				{
					document.getElementById("total").value=res;
				}
			else
				{
					document.getElementById("total").value=0;
				}
		}
	</script>
	
	
	
	
	



<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<!-- top-header and slider -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
<script src="js/minicart.min.js"></script>
<script>
	// Mini Cart
	paypal.minicart.render({
		action: '#'
	});

	if (~window.location.search.indexOf('reset=true')) {
		paypal.minicart.reset();
	}
</script>
<!-- main slider-banner -->
<script src="js/skdslider.min.js"></script>
<link href="css/skdslider.css" rel="stylesheet">
<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery('#demo1').skdslider({'delay':5000, 'animationSpeed': 2000,'showNextPrev':true,'showPlayButton':true,'autoSlide':true,'animationType':'fading'});
						
			jQuery('#responsive').change(function(){
			  $('#responsive_wrapper').width(jQuery(this).val());
			});
			
		});
</script>	
<!-- //main slider-banner --> 
</body>
</html>