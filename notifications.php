<!--
author: W3layouts
author URL: http: //w3layouts.com
	License: Creative Commons Attribution 3.0 Unported
License URL: http: //creativecommons.org/licenses/by/3.0/
	-->
	<?php

include( "db.php" );
$uid = $_SESSION[ 'logid' ];
?>
<!DOCTYPE html>
<html>

<head>
	<title>SMART SHOPPEE</title>
	<!-- for-mobile-apps -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="keywords" content="Super Market Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design"/>
	<script type="application/x-javascript">
		addEventListener( "load", function () {
			setTimeout( hideURLbar, 0 );
		}, false );

		function hideURLbar() {
			window.scrollTo( 0, 1 );
		}
	</script>
	<!-- //for-mobile-apps -->
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>
	<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
	<!-- font-awesome icons -->
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- //font-awesome icons -->
	<!-- js -->
	<script src="js/jquery-1.11.1.min.js"></script>
	<!-- //js -->
	<link href='//fonts.googleapis.com/css?family=Raleway:400,100,100italic,200,200italic,300,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
	<!-- start-smoth-scrolling -->
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
		jQuery( document ).ready( function ( $ ) {
			$( ".scroll" ).click( function ( event ) {
				event.preventDefault();
				$( 'html,body' ).animate( {
					scrollTop: $( this.hash ).offset().top
				}, 1000 );
			} );
		} );
	</script>
	<!-- start-smoth-scrolling -->
</head>

<body>
	<!-- header -->
	<div class="agileits_header">
		<div class="container">
			<div class="w3l_offers">
				<p>Mega offer sales started....Dont miss it..... <a href="products.html">SHOP NOW</a>
				</p>
			</div>
			<div class="agile-login">
				<ul>
					<li><a href="my_cart.php"> My orders </a>
					</li>
					<li><a href="logout.php">Logout</a>
					</li>
					<li><a href="faq.php">Help</a>
					</li>

					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account<b class="caret"></b></a>
						<ul class="dropdown-menu multi-column columns-3">
							<div class="row">
								<div class="multi-gd-img">
									<ul class="multi-column-dropdown">

										<li class="dropdown"><a href="profile.php"> View Profile </a>
										</li>
										<li><a href="notifications.php"> Notifications </a>
										</li>
										<li><a href="smart_wallet.php"> Smart Wallet </a>
										</li>
										<li><a href="mypurchase.php">My Purchase</a>
										</li>
										<li><a href="contact.php">Contact Us</a>
										</li>

									</ul>
								</div>

							</div>
						</ul>
					</li>



				</ul>
			</div>
			<div class="product_list_header">
				<form action="#" method="post" class="last">
					<input type="hidden" name="cmd" value="_cart">
					<input type="hidden" name="display" value="1">
				</form>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<div class="logo_products">
		<div class="container">
			<div class="w3ls_logo_products_left1">
				<ul class="phone_email">
					<li><i class="fa fa-phone" aria-hidden="true"></i>Order online or call us : +91 9061230650</li>
				</ul>
			</div>
			<div class="w3ls_logo_products_left">
				<h1><a href="user_home.php">SMART SHOPPEE</a></h1>
			</div>


			<div class="clearfix"> </div>
		</div>
	</div>
	<!-- //header -->
	<!-- navigation -->
	<div class="navigation-agileits">
		<div class="container">
			<nav class="navbar navbar-default">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header nav_2">
					<button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
				

				</div>





				<div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
					<ul class="nav navbar-nav">
						<!--<li class="active"><a href="user_home.php" class="act">Home</a></li>	-->
						<!-- Mega Menu -->

						<li><a href="user_home.php">Home</a>
						</li>
						<?php
						include( "user_header.php" );
						?>
					

					</ul>
				</div>
			</nav>
		</div>
	</div <!--// navigation -->




	<!-- main-slider -->
	<div class="top-brands">
		<div class="container">

			<h2>Your Notifications</h2><br>

			<?php
			$flag = 0;
			$e = "select fname from user_registration where user_id=$uid";
			$u = mysqli_query( $con, $e );
			$w = mysqli_fetch_array( $u );

			$q = "SELECT * FROM complaints where user_id='10007' and reply!='pending'";
			$exe = mysqli_query( $con, $q );
			$g = mysqli_num_rows( $exe );
			if ( $g > 0 ) {

				$flag = 1;
				?>

			<table class="timetable_sub" border="2" bordercolordark="#570002">
				<thead>

					<tr>
						<th>Your Complaint</th>
						<th>Reply</th>



					</tr>
				</thead>


				<?php

				while ( $r = mysqli_fetch_array( $exe ) ) {
					?>
				<tr>
					<td>
						<?php echo $r['complaint_details'];?>
					</td>
					<td>
						<?php echo $r['reply'];?>
					</td>




				</tr>
				<?php



				}


				?>
			</table><br>
			<?php
			}



			$rr = "select * from order_tbl where uid=$uid";
			$t = mysqli_query( $con, $rr );
			while ( $m = mysqli_fetch_array( $t ) ) {
				$flag = 1;
				?>
			<table class="timetable_sub" border="2" bordercolordark="#570002" <thead>
				<tr>
					<th>Your order with order id
						<?php echo($m['oid']); ?> is Done with Status
						<?php echo($m['stat']); ?> <br> *********</th>
				</tr>
				</thead>

			</table><br>

			<?php
			}

			?>

			<?php
			$l = "select * from `return` where uid='$uid' order by status desc";
			$n = mysqli_query( $con, $l );
			$h = mysqli_num_rows( $n );
			if ( $h > 0 ) {
				while ( $m = mysqli_fetch_array( $n ) ) {
					?>
			<table class="timetable_sub" border="2" bordercolordark="#570002" <thead>
				<tr>
					<th>Your Return with id
						<?php echo($m['rid']); ?> is Done with Status
						<?php echo($m['status']); ?> <br>
						<?php
						if ( $m[ 'status' ] == 'Rejected' ) {
							?>
						<p>Due to </p>
						<?php echo($m['rjct_rsn']); ?> <br> *********</th>
					<?php
					}

					?>
				</tr>
				</thead>

			</table><br>
			<?php
			}

			}

			if ( $flag == 0 ) {

				echo "Currently You Have No Reply";
			}
			?>

		</div>
	</div>










	<!-- //top-brands -->
	<!-- Carousel
    ================================================== -->
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>
			<li data-target="#myCarousel" data-slide-to="2"></li>
		</ol>
		<div class="carousel-inner" role="listbox">
			<div class="item active">
				<a href="beverages.html"> <img class="first-slide" src="images/b1.jpg" alt="First slide"></a>

			</div>
			<div class="item">
				<a href="personalcare.html"> <img class="second-slide " src="images/b3.jpg" alt="Second slide"></a>

			</div>
			<div class="item">
				<a href="household.html"><img class="third-slide " src="images/b1.jpg" alt="Third slide"></a>

			</div>
		</div>

	</div>
	<!-- /.carousel -->
	<!--banner-bottom-->
	<div class="ban-bottom-w3l">
		<div class="container">
			<div class="col-md-6 ban-bottom3">
				<div class="ban-top">
					<img src="images/p2.jpg" class="img-responsive" alt=""/>

				</div>
				<div class="ban-img">
					<div class=" ban-bottom1">
						<div class="ban-top">
							<img src="images/p3.jpg" class="img-responsive" alt=""/>

						</div>
					</div>
					<div class="ban-bottom2">
						<div class="ban-top">
							<img src="images/p4.jpg" class="img-responsive" alt=""/>

						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="col-md-6 ban-bottom">
				<div class="ban-top">
					<img src="images/111.jpg" class="img-responsive" alt=""/>


				</div>
			</div>

			<div class="clearfix"></div>
		</div>
	</div>
	<!--banner-bottom-->
	<!--brands-->



	<!--//brands-->
	<!-- new -->


	<!-- //new -->
	<!-- //footer -->


	<!-- //footer -->
	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.min.js"></script>

	<!-- top-header and slider -->
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$( document ).ready( function () {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/

			$().UItoTop( {
				easingType: 'easeOutQuart'
			} );

		} );
	</script>
	<!-- //here ends scrolling icon -->
	<script src="js/minicart.min.js"></script>
	<script>
		// Mini Cart
		paypal.minicart.render( {
			action: '#'
		} );

		if ( ~window.location.search.indexOf( 'reset=true' ) ) {
			paypal.minicart.reset();
		}
	</script>
	<!-- main slider-banner -->
	<script src="js/skdslider.min.js"></script>
	<link href="css/skdslider.css" rel="stylesheet">
	<script type="text/javascript">
		jQuery( document ).ready( function () {
			jQuery( '#demo1' ).skdslider( {
				'delay': 5000,
				'animationSpeed': 2000,
				'showNextPrev': true,
				'showPlayButton': true,
				'autoSlide': true,
				'animationType': 'fading'
			} );

			jQuery( '#responsive' ).change( function () {
				$( '#responsive_wrapper' ).width( jQuery( this ).val() );
			} );

		} );
	</script>
	<!-- //main slider-banner -->
</body>

</html>