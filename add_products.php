<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php 
include("db.php");
?>
<!DOCTYPE html>
<html>
<head>
<title>Smart Shoppee</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Super Market Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- font-awesome icons -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<!-- js -->
<script src="js/jquery-1.11.1.min.js"></script>
<!-- //js -->
<link href='//fonts.googleapis.com/css?family=Raleway:400,100,100italic,200,200italic,300,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<style>
.autodropdown{
	position:absolute;
	top:530px;
	left:145px;
}
.suggestresult{
	width:300px;
	list-style:none;
}
 
.suggestresult li{
	padding:5px;
	border:1px solid #333;
	border-top:0;
	cursor:pointer;
	background:#333;
	color:#fff;
	border-bottom:1px solid #666;
}

.suggestresult li:hover{
	background:#666;
	color:#ffffff;
	cursor:pointer;
}

</style>
<script src="autosuggestion.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="mini.js"></script>
<script>
$(document).ready(function(){

$("#select").click(function(){
	
var thiss=$(this);
var value=thiss.val();
$.get("get_subcat.php",{id:value},function(data){
$("#abc").html(data);
});
});
});
</script>




<script>
$(document).ready(function(){
	$('.abhijitscript').keyup(function(){
		var query_string = $.trim($(this).val());
		//alert(query_string.length);
		if(query_string != ''){
			$.ajax({
			type: "POST",
			url: "getItem_3.php",
			data: { name:query_string },
			success: function(data)
			{
				$('.suggestresult').html(data);
				$('.suggestresult li').click(function(){
					var return_value = $(this).text();
					
					
					$('.abhijitscript').attr('value', return_value); 
					$('.abhijitscript').val(return_value);
					$('.suggestresult').html('');
					
					//auto data get starting 
					var inp=return_value;
					//alert(inp);                    
					var data = {
						"action": inp
					};
						data = $(this).serialize() + "&" + $.param(data);
						$.ajax({		
									type: "POST",
									dataType: "json",
									url: "getdata1.php", //Relative or absolute path to response.php file
									data: data,
									success: function(data) {     
										$("#gst").val(data["gst"]);
										
										document.getElementById("gst").focus();
									}
								});
								return false;
					//auto gata get ending
					
					
				});
			}
		});
		}
	});
});
</script>







<script type="text/javascript">
    // Restrict user input in a text field
    // create as many regular expressions here as you need:
    var digitsOnly = /[1234567890/]/g;
    var integerOnly = /[0-9\.]/g;
    var alphaOnly = /[A-Za-z\._-12345]/g;
    var usernameOnly = /[A-Za-z\._-]/g;

    function restrictInput(myfield, e, restrictionType, checkdot){
        if (!e) var e = window.event
        if (e.keyCode) code = e.keyCode;
        else if (e.which) code = e.which;
        var character = String.fromCharCode(code);

        // if user pressed esc... remove focus from field...
        if (code==27) { this.blur(); return false; }

        // ignore if the user presses other keys
        // strange because code: 39 is the down key AND ' key...
        // and DEL also equals .
        if (!e.ctrlKey && code!=9 && code!=8 && code!=36 && code!=37 && code!=38 && (code!=39 || (code==39 && character=="'")) && code!=40) 
		{
            if (character.match(restrictionType)) 
			{
                if(checkdot == "checkdot")
				{
                    return !isNaN(myfield.value.toString() + character);
                } 
				else 
				{
                    return true;
                }
            } 
			else 
			{
                return false;
            }
        }
    }
</script>
<script>
	
		
		function tot()
		{
			var price=document.getElementById("price").value;
			var gst=document.getElementById("gst").value;
			
			var res=(parseFloat(price)*parseFloat(gst))/100;
			var res1=parseFloat(price)+res;
			if(!isNaN(res))
				{
					document.getElementById("mrp").value=res1;
				}
			else
				{
					document.getElementById("mrp").value=0;
				}
		}
	</script>
<!-- start-smoth-scrolling -->
</head>
	
<body>
<!-- header -->
	<div class="agileits_header">
		<div class="container">
			
			<div class="agile-login">
				
			</div>
			<div class="product_list_header">  
					<form action="#" method="post" class="last"> 
						<input type="hidden" name="cmd" value="_cart">
						<input type="hidden" name="display" value="1">
					</form>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>

	<div class="logo_products">
		<div class="container">
		<div class="w3ls_logo_products_left1">
		<!--	<li><i class="fa fa-phone" aria-hidden="true"></i>Order online or call us : +99 9061230650</li> -->
				
			
			</div>
			<div class="w3ls_logo_products_left">
				<h1><a href="admin_home.php">SMART SHOPPEE</a></h1>
			</div>
		
			
			<div class="clearfix"> </div>
		</div>
	</div>
<!-- //header -->
<!-- navigation -->
		<div class="navigation-agileits">
		<div class="container">
			<nav class="navbar navbar-default">
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header nav_2">
								<button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div> 
							
							
							
							
							
							<div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
								<ul class="nav navbar-nav">
									<!--<li class="active"><a href="employee_home.php" class="act">Home</a></li>	-->
									<!-- Mega Menu -->
									
									<li><a href="billing.php">New Bill</a></li>
									<li><a href="old_bill.php">Old Bill</a></li>	
									<li><a href="add_products.php">Add New Products</a></li>
														
									<li><a href="product_management.php">Manage</a></li>
									<li><a href="product_management.php">Update Stock</a></li>					
											
									
									
										<li><a href="online_sales.php">View  Sales</a></li>
														<li><a href="order.php">View Orders</a></li>
														<li><a href="verify_return.php">View Return</a></li>
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">Settings<b class="caret"></b></a>
										<ul class="dropdown-menu multi-column columns-3">
											<div class="row">
												<div class="multi-gd-img">
													<ul class="multi-column-dropdown">
														
														
														<li><a href="\smart shoppee\logout.php">Logout</a></li>
													</ul>
												</div>	
												
											</div>
										</ul>
									</li>
								</ul>
							</div>
							</nav>
			</div>
		</div>
<!-- //navigation -->
<!-- breadcrumbs -->
	<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="employee_home.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
				<li class="active">Register Page</li>
			</ol>
		</div>
	</div>
<!-- //breadcrumbs -->
<!-- register -->
	<div class="register">
		<div class="container">
			<h2>Register Here</h2>
			<div class="login-form-grids">
				<h6>Products Registration</h6>
					<form method="post" enctype="multipart/form-data">
						
					<!--<input type="text" placeholder="" required=" " >-->
					<select name="cat" id="select">
						<option value="select">---Select category---</option>
						<?php 
						
						$str="select * from category";
						$exe=mysqli_query($con,$str);
						while($ar=mysqli_fetch_array($exe))
						{
						?>
						<option value="<?php echo $ar['cat_name']; ?>"><?php echo $ar['cat_name']; ?></option>
						<?php
						}
						?>
					</select><br>

			    <span id="abc"></span>
			    <br>
				    
					    <input type="text" name="pname" placeholder="Product name" id="product_name" autocomplete="off" class="abhijitscript" autofocus >
				<div class="autodropdown">
				<ul class="suggestresult"></ul>
				</div>
				</br>
				<input type="number" name="gst" placeholder="GST" id="gst" class="form-control"  ></br>
						<input type="text" placeholder="Model Name..."  name="model" ></br>
						<input type="text" placeholder="Type..."  name="type" ></br>
						<input type="text" placeholder="Container Type..."  name="ctype" ></br>
						<input type="text" placeholder="Fragrance Type..."  name="ftype" ></br>
						<textarea placeholder="Description..." required name="desc" ></textarea></br>
						<input type="text" placeholder="Search Keyword..."  name="keyword" ></br>
						<input type="text" placeholder="Nozzle..."  name="nozzle" ></br>
						<textarea placeholder="Usage instruction..."  name="usage" ></textarea></br>
						<input type="text" placeholder="Texture..."  name="texture" ></br>
						<input type="text" placeholder="Color..."  name="color" ></br>
						<input type="text" placeholder="Grain Size..."  name="gsize" ></br>
						<input type="text" placeholder="Organic..."  name="org" ></br>
						<input type="text" placeholder="Maximum Self Life..."  name="life" ></br>
						<input type="text" placeholder="Origin..."  name="orign" ></br>
						<input type="text" placeholder="Dietry Preference..."  name="preferene" ></br>
						<input type="number" placeholder="Weight..."  class="form-control" name="weight" onkeypress="return restrictInput(this, event, digitsOnly);"></br>
						<input type="number" placeholder="Price..." name="price" id="price" class="form-control" onKeyUp="tot();"></br>
						<input type="text" name="mrp" placeholder="MRP.." id="mrp" value="0" readonly ></br>
						
						<input type="file"  required name="file" ></br>
						
						
						
						<input type="text" placeholder="Manufacturing Date..."  onkeypress="return restrictInput(this, event, digitsOnly);" maxlength="10" minlength="10" class="form-control" name="mfd" ></br>
						<input type="text" placeholder="Expiry Date..."  onkeypress="return restrictInput(this, event, digitsOnly);" maxlength="10" minlength="10" class="form-control" name="exp" ></br>
						<input type="text" placeholder="Other Details..."  name="other" ></br>
						<input type="text" placeholder="Warranty..."  onkeypress="return restrictInput(this, event, digitsOnly);" maxlength="50" minlength="2" class="form-control" name="warranty" ></br>
						<input type="number" placeholder="Quantity..." required onkeypress="return restrictInput(this, event, digitsOnly);" maxlength="19" minlength="5" class="form-control" name="quantity" ></br>
					
					<input type="submit" name="register" value="Register">
				</form>
				<?php
					if(isset($_POST['register']))
					{
						$pname=$_POST['pname'];
						$p="select * from item where name=$pname";
						$t=mysqli_query($con,$p);
						$s=mysqli_fetch_array($t);
						if($s>0)
						{
							echo("The product with $pname Is already Registered");
						}
						else
						{
							$file=$_POST['file'];
						$catname=$_POST['cat'];
						$scatname=$_POST['s_cat'];
						$brand=$_POST['brand'];
						$model=$_POST['model'];
						$type=$_POST['type'];
						$ctype=$_POST['ctype'];
						$ftype=$_POST['ftype'];
						$desc=$_POST['desc'];
						$keyword=$_POST['keyword'];
						$nozzle=$_POST['nozzle'];
						$usage=$_POST['usage'];
						$texture=$_POST['texture'];
						$color=$_POST['color'];
						$gsize=$_POST['gsize'];
						$org=$_POST['org'];
						$life=$_POST['life'];
						$origin=$_POST['orign'];
						$preference=$_POST['preferene'];
						$weight=$_POST['weight'];
						$price=$_POST['mrp'];
						//$gst=$_POST['mrp'];
						$mfd=$_POST['mfd'];
						$exp=$_POST['exp'];
						$other=$_POST['other'];
						$desc=$_POST['desc'];
						$warrenty=$_POST['warranty'];
						$quantity=$_POST['quantity'];
						
						$file=$_FILES["file"]["name"]; 
						$folder="C:/wamp64/www/smart shoppee/images/";
						move_uploaded_file($_FILES["file"]["tmp_name"], "$folder".$_FILES["file"]["name"]);
						
						
						$sql="select max(item_id) as mx from item";
						$exe=mysqli_query($con,$sql);
						$r=mysqli_fetch_array($exe);
						
						$max=$r['mx'];
						
						if($max==0)
						{
							$maxid=10001;
						}
						else
						{
							$maxid=$max+1;
						}
						
						
						$sql1="INSERT INTO `item`(`item_id`, `cat_name`, `sub_cat_name`, `name`, `brand`, `model`, `type`, `container_type`, `fragrence_type`, `description`, `keyword`, `nozzle`, `usage`, `texture`, `color`, `gsize`, `organic`, `max_shelf_life`, `origin`, `dietry`, `weight`, `price`, `image`, `qr`, `other`, `warranty`) VALUES ('$maxid','$catname','$scatname','$pname','$brand','$model','$type','$ctype','$ftype','$desc','$keyword','$nozzle','$usage','$texture','$color','$gsize','$org','$life','$origin','$preference','$weight','$price','$file','pending','$other','$warrenty')";
						
						$exe1=mysqli_query($con,$sql1);
						
						$sql="select max(pid) as mx from product";
						$exe=mysqli_query($con,$sql);
						$r=mysqli_fetch_array($exe);
						
						$max=$r['mx'];
						
						if($max==0)
						{
							$maxid2=10001;
						}
						else
						{
							$maxid2=$max+1;
						}
						
						
						$sql2="INSERT INTO `product`(`pid`, `item_id`, `mfd`, `exp`, `quantity`) VALUES ('$maxid2','$maxid','$mfd','$exp','$quantity')";
						$exe2=mysqli_query($con,$sql2);
						
						if($exe1 && $exe2)
						{
							?>
							<script>
								alert("product registration successfull");
								
								
							</script>
							<?php
						}
						else
						{
							echo("an error uccured while registration");
							
						}
						
					}
					}
					?>
			</div>
			<!--<div class="register-home">
				<a href="employee_home.php">Home</a>
			</div>-->
		</div>
	</div>
	
	
<!-- //register -->
<!-- //footer -->

	
		
	
<!-- //footer -->	
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
<!-- top-header and slider -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
<script src="js/minicart.min.js"></script>
<script>
	// Mini Cart
	paypal.minicart.render({
		action: '#'
	});

	if (~window.location.search.indexOf('reset=true')) {
		paypal.minicart.reset();
	}
</script>
<!-- main slider-banner -->
<script src="js/skdslider.min.js"></script>
<link href="css/skdslider.css" rel="stylesheet">
<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery('#demo1').skdslider({'delay':5000, 'animationSpeed': 2000,'showNextPrev':true,'showPlayButton':true,'autoSlide':true,'animationType':'fading'});
						
			jQuery('#responsive').change(function(){
			  $('#responsive_wrapper').width(jQuery(this).val());
			});
			
		});
</script>	
<!-- //main slider-banner --> 

</body>

</html>