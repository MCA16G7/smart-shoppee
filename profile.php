<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php
include("db.php");
$lid=$_SESSION['logid'];
?>
<!DOCTYPE html>
<html>
<head>
<title>Smart Shoppee</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Super Market Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- font-awesome icons -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<!-- js -->
<script src="js/jquery-1.11.1.min.js"></script>
<!-- //js -->
<link href='//fonts.googleapis.com/css?family=Raleway:400,100,100italic,200,200italic,300,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>


<script type="text/javascript">
    // Restrict user input in a text field
    // create as many regular expressions here as you need:
    var digitsOnly = /[1234567890]/g;
    var integerOnly = /[0-9\.]/g;
    var alphaOnly = /[A-Za-z]/g;
    var usernameOnly = /[A-Za-z\._-]/g;

    function restrictInput(myfield, e, restrictionType, checkdot){
        if (!e) var e = window.event
        if (e.keyCode) code = e.keyCode;
        else if (e.which) code = e.which;
        var character = String.fromCharCode(code);

        // if user pressed esc... remove focus from field...
        if (code==27) { this.blur(); return false; }

        // ignore if the user presses other keys
        // strange because code: 39 is the down key AND ' key...
        // and DEL also equals .
        if (!e.ctrlKey && code!=9 && code!=8 && code!=36 && code!=37 && code!=38 && (code!=39 || (code==39 && character=="'")) && code!=40) 
		{
            if (character.match(restrictionType)) 
			{
                if(checkdot == "checkdot")
				{
                    return !isNaN(myfield.value.toString() + character);
                } 
				else 
				{
                    return true;
                }
            } 
			else 
			{
                return false;
            }
        }
    }
</script>


<!-- start-smoth-scrolling -->
</head>
	
<body>
<!-- header -->
	<div class="agileits_header">
		<div class="container">
			<div class="w3l_offers">
				<p>Mega offer sales started....Dont miss it..... <a href="user_home.php">SHOP NOW</a></p>
			</div>
			<div class="agile-login">
				<ul>
					<li><a href="my_cart.php"> My orders </a></li>
					<li><a href="logout.php">Logout</a></li>
					<li><a href="faq.php">Help</a></li>
					
					
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account<b class="caret"></b></a>
										<ul class="dropdown-menu multi-column columns-3">
											<div class="row">
												<div class="multi-gd-img">
													<ul class="multi-column-dropdown">
														
														<li class="dropdown"><a href="profile.php"> View Profile </a></li>
														<li><a href="notifications.php"> Notifications </a></li>
														<li><a href="smart_wallet.php"> Smart Wallet </a></li>
														<li><a href="mypurchase.php">My Purchase</a></li>
														<li><a href="contact.php">Contact Us</a></li>
														
													</ul>
												</div>	
												
											</div>
										</ul>
									</li>
					
					
					
					
				</ul>
			</div>
		
			<div class="clearfix"> </div>
		</div>
	</div>

	<div class="logo_products">
		<div class="container">
		<div class="w3ls_logo_products_left1">
				<ul class="phone_email">
					<li><i class="fa fa-phone" aria-hidden="true"></i>Order online or call us : +91 9061230650</li>
					
				</ul>
			</div>
			<div class="w3ls_logo_products_left">
				<h1><a href="user_home.php">SMART SHOPPEE</a></h1>
			</div>
	
			
			<div class="clearfix"> </div>
		</div>
	</div>
<!-- //header -->
<!-- navigation -->
	<div class="navigation-agileits">
		<div class="container">
			<nav class="navbar navbar-default">
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header nav_2">
								<button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div> 
							<div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
								<ul class="nav navbar-nav">
									<li class="active"><a href="user_home.php" class="act">Home</a></li>	
									<!-- Mega Menu -->
									<?php
									include("user_header.php");
									?>
									
									
								</ul>
							</div>
							</nav>
			</div>
		</div>
		
<!-- //navigation -->
<!-- breadcrumbs -->
	<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="user_home.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
				<li class="active">Register Page</li>
			</ol>
		</div>
	</div>
<!-- //breadcrumbs -->
<!-- register -->
	<div class="register">
		<div class="container">
			<h2>my profile</h2>
			<div class="login-form-grids">
			
					<?php /*?><h5>profile information</h5><?php */?>
				<?php /*?><form action="#" method="post">
					<input type="text" placeholder="First Name..." required=" " >
					<input type="text" placeholder="Last Name..." required=" " >
				</form>
				<div class="register-check-box">
					<div class="check">
						<label class="checkbox"><input type="checkbox" name="checkbox"><i> </i>Subscribe to Newsletter</label>
					</div>
				</div><?php */?>
				
				
				
				<?php
				$qry="select * from user_registration u,login l where u.user_id='$lid' and u.user_id=l.log_id";
				$run=mysqli_query($con,$qry);
				$r=mysqli_fetch_array($run);
				?>
				
				<h6>Register Here</h6>
					<form method="post">
					<!--<input type="text" placeholder="" required=" " >-->
						<p>First Name</p><input type="text"  value="<?php echo $r['fname'];?>"  required class="form-control"  onkeypress="return restrictInput(this, event, alphaOnly);" name="fname"></br>
						<p>Last Name</p><input type="text"  value="<?php echo $r['lname'];?>" required class="form-control"  onkeypress="return restrictInput(this, event, alphaOnly);"  name="lname" ></br>
						<p>Address</p><textarea  required  name="addr" ><?php echo $r['address'];?></textarea></br>
						<p>Email</p><input type="email"  required=" " name="email" value="<?php echo $r['email'];?>" ></br>
					
						<p>Phone</p><input type="text"  value="<?php echo $r['phone'];?>" required onkeypress="return restrictInput(this, event, digitsOnly);"  maxlength="10"  class="form-control" name="phone" ></br>
						<p>User Name</p><input type="text"  required  name="unm" value="<?php echo $r['user_name'];?>" ></br>
										<p>Password</p><input type="password"  required  name="psw" value="<?php echo $r['password'];?>" ></br>
											
						<input type="submit" name="register" value="UPDATE">
				</form>
				<?php
					if(isset($_POST['register']))
					{
						$fname=$_POST['fname'];
						$lname=$_POST['lname'];
						$addr=$_POST['addr'];
						$email=$_POST['email'];
						$phone=$_POST['phone'];
						$unm=$_POST['unm'];
						$psw=$_POST['psw'];
						
						
						
						
						
						$sql1="update `user_registration` set `fname`='$fname', `lname`='$lname', `email`='$email', `address`='$addr', `phone`='$phone' where user_id='$lid'";
						$exe1=mysqli_query($con,$sql1);
						
						
						
						$sql2="update `login` set `user_name`='$unm' , password='$psw' where log_id='$lid'";
						$exe2=mysqli_query($con,$sql2);
						
						if($exe2 && $exe1)
						{
							?>
							<script>
								alert("Profile Updated " );
								window.location.assign("profile.php");
							</script>
							<?php
						}
						else{
							echo("An error occured !!!!!!!");
							
						}
						
					}
					?>
			</div>
			<div class="register-home">
				<a href="user_home.php">Home</a>
			</div>
		</div>
	</div>
	
	
<!-- //register -->
<!-- //footer -->

	
	
	
	
	
	
	
	
	
	
	
	
<div class="footer">
		<div class="container">
			<div class="w3_footer_grids">
				<div class="col-md-3 w3_footer_grid">
					
					
					<ul class="address">
						<li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>Smart Shoppee, 4th block, <span>New York City.</span></li>
						<li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:smartshoppeekmct@gmail.com">smartshoppeekmct@gmail.com</a></li>
						
					</ul>
				</div>
				
				
				<div class="col-md-3 w3_footer_grid">
					
					<ul class="info"> 
					<li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="about_us.php">About Us</a></li>
						<li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="contact.php">Contact Us</a></li>
					
						
						
					</ul>
					
					
				</div>
				<div class="col-md-3 w3_footer_grid">
					
					<ul class="info"> 
					
					<li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="faq.php">FAQ's</a></li>
						<li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="my_cart.php">My Cart</a></li>
						
						
					</ul>
					
					
				</div>
				<div class="col-md-3 w3_footer_grid">
					
					<ul class="info"> 
					<li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="feedback.php">Feedback</a></li>
						<li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="user_complaints.php">Complaints</a></li>
						
					</ul>
					
					
				</div>
				
				<div class="clearfix"> </div>
			</div>
		</div>
		
		<div class="footer-copy">
			
			<div class="container">
				<!--<p>© 2017 Super Market. All rights reserved | Design by <a href="http://w3layouts.com/">W3layouts</a></p>-->
			</div>
		</div>
		
	</div>	
	<div class="footer-botm">
			<div class="container">
				<div class="w3layouts-foot">
					<ul>
						<li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
						<li><a href="#" class="w3_agile_vimeo"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
					</ul>
				</div>
				<div class="payment-w3ls">	
					<img src="images/card.png" alt=" " class="img-responsive">
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
<!-- //footer -->	
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
<!-- top-header and slider -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
<script src="js/minicart.min.js"></script>
<script>
	// Mini Cart
	paypal.minicart.render({
		action: '#'
	});

	if (~window.location.search.indexOf('reset=true')) {
		paypal.minicart.reset();
	}
</script>
<!-- main slider-banner -->
<script src="js/skdslider.min.js"></script>
<link href="css/skdslider.css" rel="stylesheet">
<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery('#demo1').skdslider({'delay':5000, 'animationSpeed': 2000,'showNextPrev':true,'showPlayButton':true,'autoSlide':true,'animationType':'fading'});
						
			jQuery('#responsive').change(function(){
			  $('#responsive_wrapper').width(jQuery(this).val());
			});
			
		});
</script>	
<!-- //main slider-banner --> 

</body>

</html>