﻿<!--
author: W3layouts
author URL: http: //w3layouts.com
	License: Creative Commons Attribution 3.0 Unported
License URL: http: //creativecommons.org/licenses/by/3.0/
	-->
	<?php

include( "db.php" );
$uid = $_SESSION[ 'logid' ];
?>
<!DOCTYPE html>
<html>

<head>
	<title>SMART SHOPPEE</title>
	<!-- for-mobile-apps -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="keywords" content="Super Market Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design"/>
	<script type="application/x-javascript">
		addEventListener( "load", function () {
			setTimeout( hideURLbar, 0 );
		}, false );

		function hideURLbar() {
			window.scrollTo( 0, 1 );
		}
	</script>
	<!-- //for-mobile-apps -->
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>
	<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
	<!-- font-awesome icons -->
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- //font-awesome icons -->
	<!-- js -->
	<script src="js/jquery-1.11.1.min.js"></script>
	<!-- //js -->
	<link href='//fonts.googleapis.com/css?family=Raleway:400,100,100italic,200,200italic,300,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
	<!-- start-smoth-scrolling -->
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
		jQuery( document ).ready( function ( $ ) {
			$( ".scroll" ).click( function ( event ) {
				event.preventDefault();
				$( 'html,body' ).animate( {
					scrollTop: $( this.hash ).offset().top
				}, 1000 );
			} );
		} );
	</script>
	<!-- start-smoth-scrolling -->
</head>

<body>
	<!-- header -->
	<div class="agileits_header">
		<div class="container">
			<div class="w3l_offers">
				<p>Mega offer sales started....Dont miss it..... <a href="user_home.php">SHOP NOW</a>
				</p>
			</div>
			<div class="agile-login">
				<ul>
					<li><a href="my_cart.php"> My orders </a>
					</li>
					<li><a href="logout.php">Logout</a>
					</li>
					<li><a href="faq.php">Help</a>
					</li>

					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account<b class="caret"></b></a>
						<ul class="dropdown-menu multi-column columns-3">
							<div class="row">
								<div class="multi-gd-img">
									<ul class="multi-column-dropdown">

										<li class="dropdown"><a href="profile.php"> View Profile </a>
										</li>
										<li><a href="notifications.php"> Notifications </a>
										</li>
										<li><a href="smart_wallet.php"> Smart Wallet </a>
										</li>
										<li><a href="mypurchase.php">My Purchase</a>
										</li>
										<li><a href="contact.php">Contact Us</a>
										</li>

									</ul>
								</div>

							</div>
						</ul>
					</li>



				</ul>
			</div>
			<div class="product_list_header">
				<form action="#" method="post" class="last">
					<input type="hidden" name="cmd" value="_cart">
					<input type="hidden" name="display" value="1">
				</form>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<div class="logo_products">
		<div class="container">
			<div class="w3ls_logo_products_left1">
				<ul class="phone_email">
					<li><i class="fa fa-phone" aria-hidden="true"></i>Order online or call us : 8089503592</li>
				</ul>
			</div>
			<div class="w3ls_logo_products_left">
				<h1><a href="user_home.php">SMART SHOPPEE</a></h1>
			</div>


			<div class="clearfix"> </div>
		</div>
	</div>
	<!-- //header -->
	<!-- navigation -->
	<div class="navigation-agileits">
		<div class="container">
			<nav class="navbar navbar-default">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header nav_2">
					<button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
				
				</div>





				<div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
					<ul class="nav navbar-nav">
						<!--<li class="active"><a href="user_home.php" class="act">Home</a></li>	-->
						<!-- Mega Menu -->

						<li><a href="user_home.php">Home</a>
						</li>
						<?php
						include( "user_header.php" );
						?>


					</ul>
				</div>
			</nav>
		</div>
	</div>
	<!-- //navigation -->
	<!-- breadcrumbs -->
	<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="index.html"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a>
				</li>
				<li class="active">Products</li>
			</ol>
		</div>
	</div>
	<!-- //breadcrumbs -->
	<!--- products --->
	<div class="products">
		<div class="container">
			<div class="col-md-4 products-right">
				<div class="categories">
					<h2 style="background-color: navajowhite">My Purchase</h2>
					<ul class="cate">
						<table class="timetable_sub" bordercolordark="#000000">
							<?php
							$s = "select * from order_tbl o,cart c,item i where o.uid='$uid' and o.stat!='carting' and o.oid=c.oid and c.pid=i.item_id group by c.idd order by o.stat desc ";
							$k = mysqli_query( $con, $s );


							while ( $r = mysqli_fetch_array( $k ) ) {
								$oid = $r[ 'oid' ];
								$pid = $r[ 'pid' ];
								$t = 0;
								?>
							<table class="timetable_sub" bordercolordark="#000000">
								<tr>
									<td>
										<img src="employee/images/<?php echo $r['image']; ?>" height="80" width="80"><br> Price :
										<?php echo($r['price']) ?><br> Quantity :
										<?php echo($r['qty']) ?><br>
										<?php $t=$t+$r['price']*$r['qty']; ?> Total Amount :
										<?php echo("$t"); ?><br>
										Payment Status : <?php echo $r['p_status']; ?>
										<br> Order Status :
										<?php echo($r['stat']) ?><br>

										<?php
										if ( $r[ 'stat' ] != 'delivered')  {
											
											if( $r['p_status']!='payed')
											{
												
											
											?>

										<h6 style="background-color: lightgreen">Arrange Payment</h6>
										<?php
											}
										}
								
										if ( $r[ 'stat' ] == 'delivered' ) {

											$sql1 = "SELECT * FROM `order_tbl` where  curdate() <= date(`del_date`)+3 and oid='$oid'";
											$exe1 = mysqli_query( $con, $sql1 );
											$c = mysqli_num_rows( $exe1 );

											if ( $c > 0 ) {
												?>
										<p style="background-color: darkgoldenrod">Can return before 3 days from Date of delivery</p>
										<a href="return.php?idd=<?php echo $r['idd'];?>"><button>Proceed to Return</button></a>
										<?php



										}
										}

										?>

									</td>

								</tr>

							</table><br>
							<?php
							}


							?>


						</table>

					</ul>
				</div>
			</div>
			<div class="col-md-8 products-right">
				<div class="agile_top_brands_grids">


					<div class="agile_top_brands_grids">



						<!--	
							dynamic product listing
							-->

						<?php



						$str = "select * from item";
						$exe = mysqli_query( $con, $str );
						while ( $ar = mysqli_fetch_array( $exe ) ) {

							?>
						<div class="col-md-4 top_brand_left">
							<div class="hover14 column">
								<div class="agile_top_brand_left_grid">
									<div class="agile_top_brand_left_grid_pos">
										<img src="images/offer.png" alt=" " class="img-responsive"/>
									</div>
									<div class="agile_top_brand_left_grid1">
										<figure>
											<div class="snipcart-item block">
												<div class="snipcart-thumb">
													<a href="product_specification.php?idd=<?php echo $ar['item_id'];?>"><img title=" " alt=" " src="employee/images/<?php echo $ar['image'];?>" /></a>
													<p>
														<?php echo $ar['name'];?>
													</p>

													<h4>Rs.<?php echo $ar['price'];?> /-</h4>
												</div>
												<div class="snipcart-details top_brand_home_details">
													<form action="#" method="post">
														<fieldset>

															<a href="add_cart.php?pid=<?php echo $ar['item_id'];?>&pnm=<?php echo $ar['name'];?>&pr=<?php echo $ar['price'];?>&k=0&cat=<?php echo $ar['sub_cat_name'];?>">ADD TO CART <img src="menu-logo-cart-hi.png" width="20" height="20">
																</fieldset>
															</form>
														</div>
													</div>
												</figure>
											</div>
										</div>
									</div>
								</div>
								<?php
								}
								?>
								
								
							</div>
					
				</div>
				<div class="agile_top_brands_grids">
					
					
						<div class="clearfix"> </div>
				</div>
				<div class="agile_top_brands_grids">
					
					
					
						<div class="clearfix"> </div>
				</div>
				
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
<!--- products --->
<!-- //footer -->
<div class="footer">
		<div class="container">
			<div class="w3_footer_grids">
				<div class="col-md-3 w3_footer_grid">
					
					
					<ul class="address">
						<li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>Smart Shoppee, 4th block, <span>New York City.</span></li>
						<li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:smartshoppeekmct@gmail.com">smartshoppeekmct@gmail.com</a>
															</li>

															</ul>
												</div>


												<div class="col-md-3 w3_footer_grid">

													<ul class="info">
														<li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="about_us.php">About Us</a>
														</li>
														<li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="contact.php">Contact Us</a>
														</li>



													</ul>


												</div>
												<div class="col-md-3 w3_footer_grid">

													<ul class="info">

														<li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="faq.php">FAQ's</a>
														</li>
														<li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="my_cart.php">My Cart</a>
														</li>


													</ul>


												</div>
												<div class="col-md-3 w3_footer_grid">

													<ul class="info">
														<li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="feedback.php">Feedback</a>
														</li>
														<li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="user_complaints.php">Complaints</a>
														</li>

													</ul>


												</div>

												<div class="clearfix"> </div>
											</div>
									</div>

									<div class="footer-copy">

										<div class="container">
											<!--<p>© 2017 Super Market. All rights reserved | Design by <a href="http://w3layouts.com/">W3layouts</a></p>-->
										</div>
									</div>

								</div>
								<div class="footer-botm">
									<div class="container">
										<div class="w3layouts-foot">
											<ul>
												<li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
												</li>
												<li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
												</li>
												<li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a>
												</li>
												<li><a href="#" class="w3_agile_vimeo"><i class="fa fa-vimeo" aria-hidden="true"></i></a>
												</li>
											</ul>
										</div>
										<div class="payment-w3ls">
											<img src="images/card.png" alt=" " class="img-responsive">
										</div>
										<div class="clearfix"> </div>
									</div>
								</div>
								<!-- //footer -->
								<!-- Bootstrap Core JavaScript -->
								<script src="js/bootstrap.min.js"></script>
								<!-- top-header and slider -->
								<!-- here stars scrolling icon -->
								<script type="text/javascript">
									$( document ).ready( function () {
										/*
											var defaults = {
											containerID: 'toTop', // fading element id
											containerHoverID: 'toTopHover', // fading element hover id
											scrollSpeed: 1200,
											easingType: 'linear' 
											};
										*/

										$().UItoTop( {
											easingType: 'easeOutQuart'
										} );

									} );
								</script>
								<!-- //here ends scrolling icon -->
								<script src="js/minicart.min.js"></script>
								<script>
									// Mini Cart
									paypal.minicart.render( {
										action: '#'
									} );

									if ( ~window.location.search.indexOf( 'reset=true' ) ) {
										paypal.minicart.reset();
									}
								</script>
								<!-- main slider-banner -->
								<script src="js/skdslider.min.js"></script>
								<link href="css/skdslider.css" rel="stylesheet">
								<script type="text/javascript">
									jQuery( document ).ready( function () {
										jQuery( '#demo1' ).skdslider( {
											'delay': 5000,
											'animationSpeed': 2000,
											'showNextPrev': true,
											'showPlayButton': true,
											'autoSlide': true,
											'animationType': 'fading'
										} );

										jQuery( '#responsive' ).change( function () {
											$( '#responsive_wrapper' ).width( jQuery( this ).val() );
										} );

									} );
								</script>
								<!-- //main slider-banner -->

</body>

</html>