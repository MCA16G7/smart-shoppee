<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php

include("db.php");

?>
<!DOCTYPE html>
<html>
<head>
<title>SMART SHOPPEE</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Super Market Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- font-awesome icons -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<!-- js -->
<script src="js/jquery-1.11.1.min.js"></script>
<!-- //js -->
<link href='//fonts.googleapis.com/css?family=Raleway:400,100,100italic,200,200italic,300,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
</head>
	
<body>
<!-- header -->
	<div class="agileits_header">
		<div class="container">
			
			<div class="agile-login">
				
			</div>
			<div class="product_list_header">  
					<form action="#" method="post" class="last"> 
						<input type="hidden" name="cmd" value="_cart">
						<input type="hidden" name="display" value="1">
					</form>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
<div class="logo_products">
		<div class="container">
		<div class="w3ls_logo_products_left1">
				<!--<ul class="phone_email">
					<li><i class="fa fa-phone" aria-hidden="true"></i>Order online or call us : +91 9061230650</li>
				</ul>-->
			</div>
			<div class="w3ls_logo_products_left">
				<h1><a href="admin_home.php">SMART SHOPPEE</a></h1>
			</div>
		<!--<div class="w3l_search">
			<form action="#" method="post">
				<input type="search" name="Search" placeholder="Search for a Product..." required="">
				<button type="submit" class="btn btn-default search" aria-label="Left Align">
					<i class="fa fa-search" aria-hidden="true"> </i>
				</button>
				<div class="clearfix"></div>
			</form>
		</div>-->
			
			<div class="clearfix"> </div>
		</div>
	</div>
<!-- //header -->
<!-- navigation -->
	<div class="navigation-agileits">
		<div class="container">
			<nav class="navbar navbar-default">
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header nav_2">
								<button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div> 
							
							
							
							
							
							<div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
								<ul class="nav navbar-nav">
									<!--<li class="active"><a href="admin_home.php" class="act">Home</a></li>	-->
									<!-- Mega Menu -->
									
									<li><a href="admin_home.php">Home</a></li>
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">Employee<b class="caret"></b></a>
										<ul class="dropdown-menu multi-column columns-3">
											<div class="row">
												<div class="multi-gd-img">
													<ul class="multi-column-dropdown">
														
														<li><a href="empl_registration.php">Add New</a></li>
														<li><a href="add_job.php">Add New Job</a></li>
														<li><a href="manage_employee.php">Manage</a></li>
														
													</ul>
												</div>	
												
											</div>
										</ul>
									</li>
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">Category<b class="caret"></b></a>
										<ul class="dropdown-menu multi-column columns-3">
											<div class="row">
												<div class="multi-gd-img">
													<ul class="multi-column-dropdown">
														
														<li><a href="add_category.php">Add New Category</a></li>
														<li><a href="add_sub_category.php">Add Sub Category</a></li>
														
														
													</ul>
												</div>	
												
											</div>
										</ul>
									</li>
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">Products<b class="caret"></b></a>
										<ul class="dropdown-menu multi-column columns-3">
											<div class="row">
												<div class="multi-gd-img">
													<ul class="multi-column-dropdown">
														
														<li><a href="add_products.php">Add New Products</a></li>
														
														<li><a href="product_management.php">Manage</a></li>
														
													</ul>
												</div>	
												
											</div>
										</ul>
									</li>
									
														
									<li><a href="sales_report.php">View Sales Reports</a></li>
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">Complaints and Feedback<b class="caret"></b></a>
										<ul class="dropdown-menu multi-column columns-3">
											<div class="row">
												<div class="multi-gd-img">
													<ul class="multi-column-dropdown">
														
														<li><a href="view_complaints.php">Complaint</a></li>
														
														<li><a href="view_feedback.php">Feedbacks</a></li>
														
													</ul>
												</div>	
												
											</div>
										</ul>
									</li>
												
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">Settings<b class="caret"></b></a>
										<ul class="dropdown-menu multi-column columns-3">
											<div class="row">
												<div class="multi-gd-img">
													<ul class="multi-column-dropdown">
														
														<li><a href="contact.php">Contacts</a></li>
														
														<li><a href="about_us.php">About us</a></li>
														
														<li><a href="\smart shoppee\logout.php">Logout</a></li>
														
													</ul>
												</div>	
												
											</div>
										</ul>
									</li>
									
								</ul>
							</div>
							</nav>
			</div>
		</div>
		
		
<!-- //navigation -->

<!-- breadcrumbs -->
	<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="user_home.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
				<li class="active">Register Page</li>
			</ol>
		</div>
	</div>
<!-- //breadcrumbs -->
<!-- register -->

<?php
	$i=$_GET['idd'];
	$str="select * from item where item_id='$i'";
	$exe=mysqli_query($con,$str);
	$ar=mysqli_fetch_array($exe);
?>
	
		<div class="products">
		<div class="container">
			<div class="agileinfo_single">
				
				<div class="col-md-4 agileinfo_single_left">
					<img id="example" src="images/<?php echo $ar['image'];?>" alt=" " class="img-responsive">
				</div>
				
				<div class="col-md-8 agileinfo_single_right">
				<h2><?php echo $ar['name'];?></h2>
					<div class="rating1">
						
						
						
					</div>
					<div class="w3agile_description">
						<h4>Product Specification</h4>
						<table class="timetable_sub">
							<tr>
								<td>Name</td><td><?php echo $ar['name'];?></td>
							</tr>
							<tr><td>Brand</td><td><?php echo $ar['brand'];?></td></tr>
								<tr><td>Description</td><td><?php echo $ar['description'];?></td></tr>
								<tr><td>Weight</td><td><?php echo $ar['weight'];?></td></tr>
								<tr><td>Color</td><td><?php echo $ar['color'];?></td></tr>
								<tr><td>Dietry</td><td><?php echo $ar['dietry'];?></td></tr>
								<tr><td>Fragrence type</td><td><?php echo $ar['fragrence_type'];?></td></tr>
								<tr><td>Grain Size</td><td><?php echo $ar['gsize'];?></td></tr>
								<tr><td>Maximum Shelf self</td><td><?php echo $ar['max_shelf_life'];?></td></tr>
								<tr><td>Model</td><td><?php echo $ar['model'];?></td></tr>
								<tr><td>Nozzle</td><td><?php echo $ar['nozzle'];?></td></tr>
								<tr><td>Organic</td><td><?php echo $ar['organic'];?></td></tr>
								<tr><td>Origin</td><td><?php echo $ar['origin'];?></td></tr>
								<tr><td>Other</td><td><?php echo $ar['other'];?></td></tr>
								<tr><td>Texture</td><td><?php echo $ar['texture'];?></td></tr>
								<tr><td>Type</td><td><?php echo $ar['type'];?></td></tr>
								<tr><td>Usage</td><td><?php echo $ar['usage'];?></td></tr>
								<tr><td>Price</td><td><?php echo $ar['price'];?></td></tr>
								<tr><td>Warranty</td><td><?php echo $ar['warranty'];?></td></tr>
						</table>
						
					</div>
					<div class="snipcart-item block">
						
						<div class="snipcart-details agileinfo_single_right_details">
							<form action="#" method="post">
								
								
									
							</form>
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>		
								
												
																				
				<div class="agile_top_brands_grids">
							
									
						
				<div class="top-brands">
		<div class="container">
		<h2>Top selling offers</h2>
			<div class="grid_3 grid_5">
				<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
					
					<div id="myTabContent" class="tab-content">
						<div role="tabpanel" class="tab-pane fade in active" id="expeditions" aria-labelledby="expeditions-tab">
							
							
							
						<!--	
							dynamic product listing
							-->
							
							<?php
								
								
								
								$str="select * from item";
								$exe=mysqli_query($con,$str);
								while($ar=mysqli_fetch_array($exe))
								{
								
								?>
								<div class="col-md-4 top_brand_left">
									<div class="hover14 column">
										<div class="agile_top_brand_left_grid">
											<div class="agile_top_brand_left_grid_pos">
												<img src="images/offer.png" alt=" " class="img-responsive" />
											</div>
											<div class="agile_top_brand_left_grid1">
												<figure>
													<div class="snipcart-item block" >
														<div class="snipcart-thumb">
															<a href="product_specification.php?idd=<?php echo $ar['item_id'];?>"><img title=" " alt=" " src="images/<?php echo $ar['image'];?>" /></a>		
															<p><?php echo $ar['name'];?></p>
															
															<h4>Rs.<?php echo $ar['price'];?> /-</h4>
														</div>
														<div class="snipcart-details top_brand_home_details">
															<form action="#" method="post">
																<fieldset>
																	
																</fieldset>
															</form>
														</div>
													</div>
												</figure>
											</div>
										</div>
									</div>
								</div>
								<?php
								}
								?>
								
								<div class="clearfix"> </div>
							</div>
							
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>		
			
			
				<!--		
				<div align="center">
					
				</div>
				
			</div>-->
			
			<!--<div class="register-home">
				<a href="user_home.php">Home</a>
			</div>-->
		<!--</div>
	</div>-->
	
	
<!-- //register -->
<!-- //footer -->

	
		
	
<!-- //footer -->	
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
<!-- top-header and slider -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
<script src="js/minicart.min.js"></script>
<script>
	// Mini Cart
	paypal.minicart.render({
		action: '#'
	});

	if (~window.location.search.indexOf('reset=true')) {
		paypal.minicart.reset();
	}
</script>
<!-- main slider-banner -->
<script src="js/skdslider.min.js"></script>
<link href="css/skdslider.css" rel="stylesheet">
<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery('#demo1').skdslider({'delay':5000, 'animationSpeed': 2000,'showNextPrev':true,'showPlayButton':true,'autoSlide':true,'animationType':'fading'});
						
			jQuery('#responsive').change(function(){
			  $('#responsive_wrapper').width(jQuery(this).val());
			});
			
		});
</script>	
<!-- //main slider-banner --> 

</body>
</html>