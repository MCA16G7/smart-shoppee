﻿<!--
author: W3layouts
author URL: http: //w3layouts.com
	License: Creative Commons Attribution 3.0 Unported
License URL: http: //creativecommons.org/licenses/by/3.0/
	-->
	<?php

include( "db.php" );
$uid = $_SESSION[ 'logid' ];
?>
<!DOCTYPE html>
<html>

<head>
	<title>SMART SHOPPEE</title>
	<!-- for-mobile-apps -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="keywords" content="Super Market Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design"/>
	<script type="application/x-javascript">
		addEventListener( "load", function () {
			setTimeout( hideURLbar, 0 );
		}, false );

		function hideURLbar() {
			window.scrollTo( 0, 1 );
		}
	</script>
	<!-- //for-mobile-apps -->
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>
	<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
	<!-- font-awesome icons -->
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- //font-awesome icons -->
	<!-- js -->
	<script src="js/jquery-1.11.1.min.js"></script>
	<!-- //js -->
	<link href='//fonts.googleapis.com/css?family=Raleway:400,100,100italic,200,200italic,300,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
	<!-- start-smoth-scrolling -->
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
		jQuery( document ).ready( function ( $ ) {
			$( ".scroll" ).click( function ( event ) {
				event.preventDefault();
				$( 'html,body' ).animate( {
					scrollTop: $( this.hash ).offset().top
				}, 1000 );
			} );
		} );
	</script>
	<!-- start-smoth-scrolling -->
</head>

<body>
	<!-- header -->
	<div class="agileits_header">
		<div class="container">
			<div class="w3l_offers">
				<p>Mega offer sales started....Dont miss it..... <a href="user_home.php">SHOP NOW</a>
				</p>
			</div>
			<div class="agile-login">
				<ul>
					<li><a href="my_cart.php"> My orders </a>
					</li>
					<li><a href="logout.php">Logout</a>
					</li>
					<li><a href="faq.php">Help</a>
					</li>


					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account<b class="caret"></b></a>
						<ul class="dropdown-menu multi-column columns-3">
							<div class="row">
								<div class="multi-gd-img">
									<ul class="multi-column-dropdown">

										<li class="dropdown"><a href="profile.php"> View Profile </a>
										</li>
										<li><a href="notifications.php"> Notifications </a>
										</li>
										<li><a href="smart_wallet.php"> Smart Wallet </a>
										</li>
										<li><a href="mypurchase.php">My Purchase</a>
										</li>
										<li><a href="contact.php">Contact Us</a>
										</li>

									</ul>
								</div>

							</div>
						</ul>
					</li>




				</ul>
			</div>
			<div class="product_list_header">
				<form action="#" method="post" class="last">
					<input type="hidden" name="cmd" value="_cart">
					<input type="hidden" name="display" value="1">
				</form>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<div class="logo_products">
		<div class="container">
			<div class="w3ls_logo_products_left1">
				<ul class="phone_email">
					<li><i class="fa fa-phone" aria-hidden="true"></i>Order online or call us : 8089503591</li>
				</ul>
			</div>
			<div class="w3ls_logo_products_left">
				<h1><a href="user_home.php">SMART SHOPPEE</a></h1>
			</div>


			<div class="clearfix"> </div>
		</div>
	</div>
	<!-- //header -->
	<!-- navigation -->
	<div class="navigation-agileits">
		<div class="container">
			<nav class="navbar navbar-default">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header nav_2">
					<button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
				
				</div>





				<div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
					<ul class="nav navbar-nav">
						<!--<li class="active"><a href="user_home.php" class="act">Home</a></li>	-->
						<!-- Mega Menu -->

						<li><a href="user_home.php">Home</a>
						</li>
						<?php
						include( "user_header.php" );
						?>


					</ul>
				</div>
			</nav>
		</div>
	</div>

	<!-- //navigation -->
	<!-- breadcrumbs -->

	<!-- //breadcrumbs -->
	<!-- contact -->
	<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="user_home.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a>
				</li>
				<li class="active">FAQ</li>
			</ol>
		</div>
	</div>



	<div class="faq-w3agile">
		<div class="container">
			<h2 class="w3_agile_header">Frequently asked questions(FAQ)</h2>
			<ul class="faq">
				<li class="item1"><a href="#" title="click here">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tempor vehicula ipsum nec ?</a>
					<ul>
						<li class="subitem1">
							<p> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore. At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident.</p>
						</li>
					</ul>
				</li>
				<li class="item2"><a href="#" title="click here">The standard Lorem Ipsum passage Etiam faucibus viverra libero vel efficitur. Ut semper nisl ut laoreet ultrices ?</a>
					<ul>
						<li class="subitem1">
							<p> Tincidunt ut laoreet dolore At vero eos et Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod consectetuer adipiscing elit, sed diam nonummy nibh euismod accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident.</p>
						</li>
					</ul>
				</li>
				<li class="item3"><a href="#" title="click here">Consectetuer adipiscing elit Etiam faucibus viverra libero vel efficitur. Ut semper nisl ut laoreet ultrices?</a>
					<ul>
						<li class="subitem1">
							<p>Dincidunt ut laoreet dolore At vero eos et Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod consectetuer adipiscing elit, sed diam nonummy nibh euismod accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident.</p>
						</li>
					</ul>
				</li>
				<li class="item4"><a href="#" title="click here">Sed diam nonummy nibh euismod Etiam faucibus viverra libero vel efficitur. Ut semper nisl ut laoreet ultrices?</a>
					<ul>
						<li class="subitem1">
							<p>At vero eos et Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod consectetuer adipiscing elit, sed diam nonummy nibh euismod accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident.</p>
						</li>
					</ul>
				</li>
				<li class="item5"><a href="#" title="click here">Euismod tincidunt laoreet Etiam faucibus viverra libero vel efficitur ?</a>
					<ul>
						<li class="subitem1">
							<p>At vero eos et Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod consectetuer adipiscing elit, sed diam nonummy nibh euismod accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident.</p>
						</li>
					</ul>
				</li>
				<li class="item6"><a href="#" title="click here">Voluptas sit aspernatur aut Ut semper nisl ut laoreet ultrices ?</a>
					<ul>
						<li class="subitem1">
							<p>At vero eos et Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod consectetuer adipiscing elit, sed diam nonummy nibh euismod accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident.</p>
						</li>
					</ul>
				</li>
				<li class="item7"><a href="#" title="click here">Donec ut quam ligula feugiat Ut semper nisl ut laoreet ultrices ?</a>
					<ul>
						<li class="subitem1">
							<p>At vero eos et Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod consectetuer adipiscing elit, sed diam nonummy nibh euismod accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident.</p>
						</li>
					</ul>
				</li>
				<li class="item8"><a href="#" title="click here">The standard Lorem Ipsum Ut semper nisl ut laoreet ultrices passage ?</a>
					<ul>
						<li class="subitem1">
							<p>Lorem ipsum dolor sit amet At vero eos et Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod consectetuer adipiscing elit, sed diam nonummy nibh euismod accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident.</p>
						</li>
					</ul>
				</li>
				<li class="item9"><a href="#" title="click here">Consectetuer adipiscing Ut semper nisl ut laoreet ultrices elit ?</a>
					<ul>
						<li class="subitem1">
							<p>Lorem ipsum dolor sit amet At vero eos et Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod consectetuer adipiscing elit, sed diam nonummy nibh euismod accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident.</p>
						</li>
					</ul>
				</li>
				<li class="item10"><a href="#" title="click here">Sed diam nonummy Ut semper nisl ut laoreet ultrices nibh euismod ?</a>
					<ul>
						<li class="subitem1">
							<p>Consectetuer adipiscing elit, sed diam nonummy nibh euismod consectetuer adipiscing elit, sed diam nonummy nibh euismod accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident.</p>
						</li>
					</ul>
				</li>
			</ul>
			<!-- script for tabs -->
			<script type="text/javascript">
				$( function () {

					var menu_ul = $( '.faq > li > ul' ),
						menu_a = $( '.faq > li > a' );

					menu_ul.hide();

					menu_a.click( function ( e ) {
						e.preventDefault();
						if ( !$( this ).hasClass( 'active' ) ) {
							menu_a.removeClass( 'active' );
							menu_ul.filter( ':visible' ).slideUp( 'normal' );
							$( this ).addClass( 'active' ).next().stop( true, true ).slideDown( 'normal' );
						} else {
							$( this ).removeClass( 'active' );
							$( this ).next().stop( true, true ).slideUp( 'normal' );
						}
					} );

				} );
			</script>
			<!-- script for tabs -->
		</div>
	</div>
	<!-- contact -->
	<div class="top-brands">
		<div class="container">
			<h2>Top selling offers</h2>
			<div class="grid_3 grid_5">
				<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">

					<div id="myTabContent" class="tab-content">
						<div role="tabpanel" class="tab-pane fade in active" id="expeditions" aria-labelledby="expeditions-tab">

							<div class="agile_top_brands_grids">



								<!--	
							dynamic product listing
							-->

								<?php



								$str = "select * from item";
								$exe = mysqli_query( $con, $str );
								while ( $ar = mysqli_fetch_array( $exe ) ) {

									?>
								<div class="col-md-4 top_brand_left">
									<div class="hover14 column">
										<div class="agile_top_brand_left_grid">
											<div class="agile_top_brand_left_grid_pos">
												<img src="images/offer.png" alt=" " class="img-responsive"/>
											</div>
											<div class="agile_top_brand_left_grid1">
												<figure>
													<div class="snipcart-item block">
														<div class="snipcart-thumb">
															<a href="product_specification.php?idd=<?php echo $ar['item_id'];?>"><img title=" " alt=" " src="images/<?php echo $ar['image'];?>" /></a>
															<p>
																<?php echo $ar['name'];?>
															</p>

															<h4>Rs.<?php echo $ar['price'];?> /-</h4>
														</div>
														<div class="snipcart-details top_brand_home_details">
															<form action="#" method="post">
																<fieldset>

																	<a href="add_cart.php?pid=<?php echo $ar['item_id'];?>&pnm=<?php echo $ar['name'];?>&pr=<?php echo $ar['price'];?>&k=0&cat=<?php echo $ar['sub_cat_name'];?>">ADD TO CART <img src="menu-logo-cart-hi.png" width="20" height="20">
																</fieldset>
															</form>
														</div>
													</div>
												</figure>
											</div>
										</div>
									</div>
								</div>
								<?php
								}
								?>
								
								<div class="clearfix"> </div>
							</div>
							
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- //footer -->
<div class="footer">
		<div class="container">
			<div class="w3_footer_grids">
				<div class="col-md-3 w3_footer_grid">
					
					
					<ul class="address">
						<li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>Smart Shoppee, 4th block, <span>New York City.</span></li>
						<li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:smartshoppeekmct@gmail.com">smartshoppeekmct@gmail.com</a>
																	</li>

																	</ul>
														</div>


														<div class="col-md-3 w3_footer_grid">

															<ul class="info">
																<li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="about_us.php">About Us</a>
																</li>
																<li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="contact.php">Contact Us</a>
																</li>



															</ul>


														</div>
														<div class="col-md-3 w3_footer_grid">

															<ul class="info">

																<li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="faq.php">FAQ's</a>
																</li>
																<li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="my_cart.php">My Cart</a>
																</li>


															</ul>


														</div>
														<div class="col-md-3 w3_footer_grid">

															<ul class="info">
																<li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="feedback.php">Feedback</a>
																</li>
																<li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="user_complaints.php">Complaints</a>
																</li>

															</ul>


														</div>

														<div class="clearfix"> </div>
													</div>
											</div>

											<div class="footer-copy">

												<div class="container">
													<!--<p>© 2017 Super Market. All rights reserved | Design by <a href="http://w3layouts.com/">W3layouts</a></p>-->
												</div>
											</div>

										</div>
										<div class="footer-botm">
											<div class="container">
												<div class="w3layouts-foot">
													<ul>
														<li><a href="" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
														</li>
														<li><a href="" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
														</li>
														<li><a href="" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a>
														</li>
														<li><a href="" class="w3_agile_vimeo"><i class="fa fa-vimeo" aria-hidden="true"></i></a>
														</li>
													</ul>
												</div>
												<div class="payment-w3ls">
													<img src="images/card.png" alt=" " class="img-responsive">
												</div>
												<div class="clearfix"> </div>
											</div>
										</div>
										<!-- //footer -->
										<!-- Bootstrap Core JavaScript -->
										<script src="js/bootstrap.min.js"></script>
										<!-- top-header and slider -->
										<!-- here stars scrolling icon -->
										<script type="text/javascript">
											$( document ).ready( function () {
												/*
													var defaults = {
													containerID: 'toTop', // fading element id
													containerHoverID: 'toTopHover', // fading element hover id
													scrollSpeed: 1200,
													easingType: 'linear' 
													};
												*/

												$().UItoTop( {
													easingType: 'easeOutQuart'
												} );

											} );
										</script>
										<!-- //here ends scrolling icon -->
										<script src="js/minicart.min.js"></script>
										<script>
											// Mini Cart
											paypal.minicart.render( {
												action: '#'
											} );

											if ( ~window.location.search.indexOf( 'reset=true' ) ) {
												paypal.minicart.reset();
											}
										</script>
										<!-- main slider-banner -->
										<script src="js/skdslider.min.js"></script>
										<link href="css/skdslider.css" rel="stylesheet">
										<script type="text/javascript">
											jQuery( document ).ready( function () {
												jQuery( '#demo1' ).skdslider( {
													'delay': 5000,
													'animationSpeed': 2000,
													'showNextPrev': true,
													'showPlayButton': true,
													'autoSlide': true,
													'animationType': 'fading'
												} );

												jQuery( '#responsive' ).change( function () {
													$( '#responsive_wrapper' ).width( jQuery( this ).val() );
												} );

											} );
										</script>
										<!-- //main slider-banner -->

</body>

</html>